<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MainController {
	/**
	 * @Route("/home")
	 */
	public function number(): JsonResponse
	{
		return new JsonResponse(['Hello Api!']);
	}
}