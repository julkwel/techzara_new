<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class RegisterController extends Controller {
	/**
	 * @Route("/api/adduser")
	 * @param Request $request
	 * @param UserService $userService
	 *
	 * @return JsonResponse
	 */
	public function adduser(Request $request, UserService $userService): JsonResponse
	{
		$body = json_decode($request->getContent(), true);
		$username = $body['username'];
		$password = $body['password'];
		$email = $body['email'];
		$provider = $body['provider'];

		$user = $userService->createUser([
			'username' => $username,
			'password' => $password,
			'email' => $email,
			'provider' => $provider,
		]);

		return new JsonResponse([
			'user' => $user->getUsername(),
		]);
	}

	/**
	 * @Route("/api/register")
	 */
	public function register(): JsonResponse
	{
		return new JsonResponse(['Hello Api!']);
	}
}
