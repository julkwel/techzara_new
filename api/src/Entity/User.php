<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=25, unique=true)
	 */
	private $username;

	/**
	 * @ORM\Column(type="string", length=64)
	 */
	private $password;

	/**
	 * @ORM\Column(type="string", length=254, unique=true)
	 */
	private $email;

	/**
	 * @ORM\Column(name="is_active", type="boolean")
	 */
	private $isActive;

	/**
	 * @ORM\Column(type="string", length=64)
	 */
	private $provider;

	/**
	 * @return mixed
	 */
	public function getProvider() {
		return $this->provider;
	}

	/**
	 * @param $provider
	 *
	 * @return $this
	 */
	public function setProvider( $provider ): self {
		$this->provider = $provider;
		return $this;
	}

	public function __construct()
	{
		$this->isActive = true;
		// may not be needed, see section on salt below
		// $this->salt = md5(uniqid('', true));
	}

	public function getEmail(): string
	{
		return $this->email;
	}
	public function setEmail(string $email): self
	{
		$this->email = $email;
		return $this;
	}

	public function getUsername(): string
	{
		return $this->username;
	}
	public function setUsername(string $username): self
	{
		$this->username = $username;
		return $this;
	}

	public function getSalt()
	{
		// you *may* need a real salt depending on your encoder
		// see section on salt below
		return null;
	}

	public function getPassword(): string
	{
		return $this->password;
	}
	public function setPassword(string $password): self
	{
		$this->password = $password;
		return $this;
	}

	public function getRoles(): array
	{
		return array('ROLE_USER');
	}

	public function eraseCredentials(): void
	{
	}

	/** @see \Serializable::serialize() */
	public function serialize(): string
	{
		return serialize(array(
			$this->id,
			$this->username,
			$this->password,
			// see section on salt below
			// $this->salt,
		));
	}

	/**
	 * @param string $serialized
	 */
	public function unserialize($serialized): void
	{
		[
			$this->id,
			$this->username,
			$this->password,
			// see section on salt below
			// $this->salt
		] = unserialize($serialized, array('allowed_classes' => false));
	}
}
