<?php
declare(strict_types=1);

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Google_Client;
use Symfony\Component\Config\Definition\Exception\ForbiddenOverwriteException;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserService {
	/**
	 * @var UserPasswordEncoderInterface
	 */
	private $passwordEncoder;

	/**
	 * @var EntityManagerInterface
	 */
	private $em;

	public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
	{
		$this->passwordEncoder = $passwordEncoder;
		$this->em = $em;
	}

	public function getGoogleUser($idToken)
	{
		$client = new Google_Client(['client_id' => getenv('GOOGLE_CLIENT_ID')]);
		$payload = $client->verifyIdToken($idToken);
		if ($payload) {
			return $payload;
		}
		throw new ForbiddenOverwriteException('Forbidden');
	}

	public function createUser(array $data): User
	{
		// extract($data);
		$username = $data['username'];
		$password = $data['password'];
		$email = $data['email'];
		$provider = $data['provider'];

		$user = $this->em->getRepository(User::class)->findOneBy([
			'email' => $email,
			'provider' => $provider,
		]);
		if ($user !== null) {
			throw new ForbiddenOverwriteException();
		}
		if ($provider === 'google') {
			$userInfo = $this->getGoogleUser($password);
			$username = $userInfo['email'];
			$email = $userInfo['email'];
			$password = $userInfo['at_hash'];
		}
		$user = new User();
		$user->setUsername( $username );
		$user->setEmail( $email );
		$user->setProvider($provider);
		$password = $this->passwordEncoder->encodePassword( $user, $password );
		$user->setPassword( $password );
		$this->em->persist( $user );
		$this->em->flush();
		return $user;
	}
}