<?php
declare(strict_types=1);

namespace App\tests;

use App\Entity\User;
use App\Service\UserService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class UserServiceTest extends TestCase
{
	public function testCreate()
	{
		$em = $this->createMock(EntityManager::class);
		$encoder = $this->createMock(UserPasswordEncoder::class);

		$userService = new UserService($encoder, $em);
		$user = $userService->createUser([
			'username' => 'toto',
			'password' => 'toto',
		]);
		$this->assertInstanceOf(User::class, $user);
	}
}