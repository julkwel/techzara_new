import React,{Component} from 'react'
import './blog.css'
import img1 from '../images/family.jpg'
import img2 from '../images/photo/partage1.jpg'
import img3 from '../images/photo/partage2.jpg'
import img4 from '../images/photo/partage3.jpg'
import img5 from '../images/photo/partage4.jpg'

export default class Blog extends Component{
    render(){
        return(
            <div className="main main-raised">
            <div className="container">
    
                <div className="section">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="card card-raised card-atelier card-background">
                                <div className="content">
                                    <h3 className="category text-info">Atelier</h3>
                                    <a href="#">
                                        <h3 className="card-title">Ateliers informatique tous les samedis de 08H a 12H</h3>
                                    </a>
                                    <p className="card-description">
                                        Atelier genre partage d'experiences entre les membres, live coding , TP , veille technologie.
                                    </p>
                                    <a href="#" className="btn btn-danger ">
                                        <i className="material-icons">format_align_left</i> Plus details
                                    </a>
                                </div>
                            </div>
                        </div>
    
                        <div className="col-md-6">
                            <div className="card card-raised card-conf card-background">
                                <div className="content">
                                    <h3 className="category text-info">Conferences</h3>
                                    <h3 className="card-title">Partage avec les intervenants externe</h3>
                                    <p className="card-description">
                                         Conferences avec les intervenants externe pour partager avec les membres leurs experiences.
                                    </p>
                                    <a href="#" className="btn btn-primary ">
                                        <i className="material-icons">format_align_left</i> Plus details
                                    </a>
                                </div>
                            </div>
                        </div>
    
                        <div className="col-md-12">
                            <div className="card card-raised card-background card-hack">
                                <div className="content">
                                    <h3 className="category text-info">Hackathon</h3>
                                    <h3 className="card-title">Front End Awards 2018</h3>
                                    <p className="card-description">
                                        Techzara est en cours de preparation de l'evenement Front End Awards 2018 a Madagascar.
                                    </p>
                                    <a href="#" className="btn btn-warning ">
                                        <i className="material-icons">subject</i> Lire l'article
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="section">
                    <h3 className="title text-center">Et aussi ...</h3>
                    <br />
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card card-plain card-blog">
                                <div className="card-image">
                                    <a href="#">
                                        <img className="img img-raised" src={img2} />
                                    </a>
                                </div>
    
                                <div className="content">
                                    <h6 className="category text-info">Atelier</h6>
                                    <h4 className="card-title">
                                        <a href="#">Travaux pratique entre les membres</a>
                                    </h4>
                                    <p className="card-description">
                                        Developpement d'une projet pendant entre les membres.<a href="#"> Plus details </a>
                                    </p>
                                </div>
                            </div>
                        </div>
    
                        <div className="col-md-4">
                            <div className="card card-plain card-blog">
                                <div className="card-image">
                                    <a href="#">
                                        <img className="img img-raised" src={img1} />
                                    </a>
                                </div>
                                <div className="content">
                                    <h6 className="category text-success">
                                        Projets
                                    </h6>
                                    <h4 className="card-title">
                                        <a href="#">Creation projets necessaire avec les membres</a>
                                    </h4>
                                    <p className="card-description">
                                        Creation des differents projet utiles avec les membres.<a href="#"> Plus details </a>
                                    </p>
                                </div>
                            </div>
                        </div>
    
                        <div className="col-md-4">
                            <div className="card card-plain card-blog">
                                <div className="card-image">
                                    <a href="#">
                                        <img className="img img-raised" src={img3} />
                                    </a>
                                </div>
    
                                <div className="content">
                                    <h6 className="category text-danger">
                                        <i className="material-icons">trending_up</i> Partage
                                    </h6>
                                    <h4 className="card-title">
                                        <a href="#">Le plus grand point</a>
                                    </h4>
                                    <p className="card-description">
                                        Techzara est une association de partage d'experiences autour de la Technologie<a href="#"> Plus details </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
    
                </div>
            </div>
            <div className="subscribe-line tz-text-center">
                <div className="containertext-center">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <h3 className="title text-center">Site en cours de maintenances!</h3>
                            <p className="description text-center">
                                Votre article ici.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}