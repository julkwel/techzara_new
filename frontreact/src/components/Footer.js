import React, { Component } from 'react';
import d1 from '../images/directionjul.jpg'
import d2 from '../images/directionsolofo.jpg'

export default class Footer extends Component {
    render(){
        return(
            <footer className="footer footer-black footer-big">
              <div className="container">
                  <div className="content">
                      <div className="row">
                          <div className="col-md-4">
                              <h5>Apropos de nous</h5>
                              <p>Techzara est une association a buts non lucratifs creer en  07 Juillet 2017. </p>
                          </div>
      
                          <div className="col-md-4">
                              <h5>Social Feed</h5>
                              <div className="social-feed">
                                  <div className="feed-line">
                                      <i className="fa fa-twitter"></i>
                                      <p>Twitter</p>
                                  </div>
                                  <div className="feed-line">
                                      <i className="fa fa-facebook-square"></i>
                                      <p>Facebook</p>
                                  </div>
                              </div>
                          </div>
      
                          <div className="col-md-4">
                              <h5>Instagram Feed</h5>
                              <div className="gallery-feed">
                                  <img src={d1} className="img img-raised img-rounded"  alt='admin' />
                                  <img src={d2} className="img img-raised img-rounded"  alt='admin' />
                              </div>
                          </div>
                      </div>
                  </div>
                  <hr/>
                  <div className="copyright text-center">
                      Copyright &copy; <script>document.write(new Date().getFullYear())</script> Techzara All Rights Reserved.
                  </div>
              </div>
          </footer>
        )
    }
}