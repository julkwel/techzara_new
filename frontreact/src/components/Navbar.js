import React, { Component } from 'react';
import logo from '../images/techz.jpg'

export default class Navbar extends Component {
    render() {
        return (
            <nav className="navbar navbar-expand-md bg-dark navbar-dark">
            <a className="navbar-brand" href="#"><img style={{ height:'25px',width:'45px',margin:'5px'}} src={logo} alt="techzara" />TECHZARA</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="collapsibleNavbar">
              <ul className="navbar-nav">
                <li className="nav-item">
                    <a href="#" className="nav-link">
                        <i className="material-icons">home</i> Accueil
                    </a>
                </li>
                <li className="dropdown nav-item" id="headerNavigationItems">
                    <a href="#" className="nav-link" id="navbardrop" data-toggle="dropdown">
                        <i className="material-icons">event</i> Activite
                    </a>
                </li>
                <li className="dropdown nav-item">
                    <a href="#" className="nav-link" data-toggle="dropdown">
                        <i className="material-icons">local_library</i> Membres
                    </a>
                </li>
                <li className="nav-item">
                    <a href="#" className="nav-link" data-toggle="modal" data-target="#myModal">
                        <i className="material-icons">supervised_user_circle</i> Se connecter
                    </a>
                </li>
                <div className="modal fade" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content" style={{color:'black'}}>
                            <div className="modal-body">
                                <div className="col-md-12"> 
                                    <div className="card card-signup">
                                        <form className="form" method="" action="">
                                            <div className="header header-primary text-center">
                                                <h4 className="card-title">Log in</h4>
                                                <div className="social-line">
                                                    <a href="#" className="btn btn-just-icon btn-simple">
                                                        <i className="fa fa-facebook-square"></i>
                                                    </a>
                                                    <a href="#" className="btn btn-just-icon btn-simple">
                                                        <i className="fa fa-twitter"></i>
                                                    </a>
                                                    <a href="#" className="btn btn-just-icon btn-simple">
                                                        <i className="fa fa-google-plus"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <p className="description text-center">Or Be Classical</p>
                                            <div className="content">
                                                <div className="input-group">
                                                    <span className="input-group-addon">
                                                        <i className="material-icons">face</i>
                                                    </span>
                                                    <input type="text" className="form-control" placeholder="Username ..." />
                                                </div>
                                                <div className="input-group">
                                                    <span className="input-group-addon">
                                                        <i className="material-icons">lock_outline</i>
                                                    </span>
                                                    <input type="password" placeholder="Password ..." className="form-control" />
                                                </div>
                                            </div>
                                            <div className="footer text-center">
                                                <a href="#" className="btn btn-primary btn-simple btn-wd btn-lg">Log In</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-danger btn-simple" data-dismiss="modal">Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>
              </ul>
            </div>  
          </nav>
        );
    }
}