import React, { Component } from 'react'
import d1 from '../images/directionjul.jpg'
import d2 from '../images/directionsolofo.jpg'
import d3 from '../images/nyantso.jpg'
import d4 from '../images/mendrika.jpg'
import d5 from '../images/ginho.jpg'
import d6 from '../images/vony.jpg'

export default class Card extends Component{
    render(){
        return(
            <div className="team-4 section-image">
                  <div className="container">
                      <div className="row">
                          <div className="col-md-12 col-md-offset-2 text-center">
                              <h2 className="title display-4 text-center">Les administrateurs </h2>
                              <h5 className="description display-6 text-center">Les administrateur et fondateur de l'association TECHZARA.</h5>
                          </div>
                      </div>
      
                      <div className="row">
      
                          <div className="col-md-4">
                              <div className="card card-profile">
                                  <div className="card-avatar">
                                      <a href="#">
                                          <img className="img" src={d1}  alt='admin'/>
                                      </a>
                                  </div>
      
                                  <div className="content">
                                      <h4 className="card-title">Julien</h4>
                                      <h6 className="category text-muted">Fondateur</h6>
                                      <p className="card-description">
                                          The purpose of technology is not to confuse the brain but to serve the body.
                                      </p>
                                      <div className="footer">
                                          <a href="#" className="btn btn-just-icon btn-linkedin btn-round"><i className="fa fa-linkedin"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-twitter btn-round"><i className="fa fa-twitter"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-dribbble btn-round"><i className="fa fa-dribbble"></i></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
      
                          <div className="col-md-4">
                              <div className="card card-profile">
                                  <div className="card-avatar">
                                      <a href="#">
                                          <img className="img" src={d2}  alt='admin'/>
                                      </a>
                                  </div>
      
                                  <div className="content">
                                      <h4 className="card-title">Solofo</h4>
                                      <h6 className="category text-muted">President</h6>
      
                                      <p className="card-description">
                                          The purpose of technology is not to confuse the brain but to serve the body.
                                      </p>
                                      <div className="footer">
                                          <a href="#" className="btn btn-just-icon btn-dribbble btn-round"><i className="fa fa-dribbble"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-twitter btn-round"><i className="fa fa-twitter"></i></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
      
                          <div className="col-md-4">
                              <div className="card card-profile">
                                  <div className="card-avatar">
                                      <a href="#">
                                          <img className="img" src={d6}  alt='admin'/>
                                      </a>
                                  </div>
      
                                  <div className="content">
                                      <h4 className="card-title">Vony</h4>
                                      <h6 className="category text-muted">Tresorerie</h6>
      
                                      <p className="card-description">
                                          The purpose of technology is not to confuse the brain but to serve the body.
                                      </p>
                                      <div className="footer">
                                          <a href="#" className="btn btn-just-icon btn-dribbble btn-round"><i className="fa fa-dribbble"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-twitter btn-round"><i className="fa fa-twitter"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-pinterest btn-round"><i className="fa fa-pinterest"></i></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
      
                          <div className="col-md-4">
                              <div className="card card-profile">
                                  <div className="card-avatar">
                                      <a href="#">
                                          <img className="img" src={d4} alt='admin' />
                                      </a>
                                  </div>
      
                                  <div className="content">
                                      <h4 className="card-title">Mendrika</h4>
                                      <h6 className="category text-muted">RH</h6>
                                      <p className="card-description">
                                          The purpose of technology is not to confuse the brain but to serve the body.
                                      </p>
                                      <div className="footer">
                                          <a href="#" className="btn btn-just-icon btn-linkedin btn-round"><i className="fa fa-linkedin"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-twitter btn-round"><i className="fa fa-twitter"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-dribbble btn-round"><i className="fa fa-dribbble"></i></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
      
                          <div className="col-md-4">
                              <div className="card card-profile">
                                  <div className="card-avatar">
                                      <a href="#">
                                          <img className="img" src={d3}  alt='admin' />
                                      </a>
                                  </div>
      
                                  <div className="content">
                                      <h4 className="card-title">Ny Antso</h4>
                                      <h6 className="category text-muted">Logistique</h6>
      
                                      <p className="card-description">
                                          The purpose of technology is not to confuse the brain but to serve the body.
                                      </p>
                                      <div className="footer">
                                          <a href="#" className="btn btn-just-icon btn-dribbble btn-round"><i className="fa fa-dribbble"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-twitter btn-round"><i className="fa fa-twitter"></i></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
      
                          <div className="col-md-4">
                              <div className="card card-profile">
                                  <div className="card-avatar">
                                      <a href="#">
                                          <img className="img" src={d5}  alt='admin' />
                                      </a>
                                  </div>
      
                                  <div className="content">
                                      <h4 className="card-title">Jorginho</h4>
                                      <h6 className="category text-muted">Logistique</h6>
      
                                      <p className="card-description">
                                          The purpose of technology is not to confuse the brain but to serve the body.    
                                      </p>
                                      <div className="footer">
                                          <a href="#" className="btn btn-just-icon btn-dribbble btn-round"><i className="fa fa-dribbble"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-twitter btn-round"><i className="fa fa-twitter"></i></a>
                                          <a href="#" className="btn btn-just-icon btn-pinterest btn-round"><i className="fa fa-pinterest"></i></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
      
                      </div>
      
                  </div>
              </div>
        )
    }
}