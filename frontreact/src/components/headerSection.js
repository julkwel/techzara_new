import React, { Component } from 'react'

export default class Header extends Component {
    render(){
        return (
            <div className="page-header header-filter" style= {{ filterColor:'black' }}>
            <div className="container">
                <div className="row">
                    <div className="col-md-12 text-center">
                        <h1 className="display-1 text-center tz-ass">Techzara Madagascar</h1>
                        <h4 className="display-6 text-center">ASSOCIATION DES JEUNES PASSIONNEES DE L'INFORMATIQUE.</h4>
                        <br />
                        <a href="/Signup" target="_blank" className="btn btn-danger btn-lg text-center">
                            <i className="fa fa-code"></i> s'inscrire
                        </a>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}