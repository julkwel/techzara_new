import React, { Component } from 'react';
import julien from '../images/directionjul.jpg'
import Navbar from './Navbar'
import Footer from './Footer'
import '../css/material-kit.css'

export default class profilPage extends Component{
    render(){
        return(
            <div>
                <Navbar />
                <div className="page-header header-filter tz-profile-page" data-parallax="active"></div>

                <div className="main main-raised tz-profile">
                    <div className="profile-content">
                        <div className="container">
                            <div className="row">
                                <div className="col s12">
                                <div className="profile">
                                        <div className="avatar">
                                            <img src={julien} alt="Circle Image" className="img-circle img-responsive img-raised" />
                                        </div>
                                        <div className="name">
                                            <h3 className="title">RAJERISON Julien</h3>
                                            <h6>Fondateur de l'association Techzara</h6>
                                            <a href="#pablo" className="btn btn-just-icon btn-simple btn-dribbble"><i className="fa fa-dribbble"></i></a>
                                            <a href="#pablo" className="btn btn-just-icon btn-simple btn-twitter"><i className="fa fa-twitter"></i></a>
                                            <a href="#pablo" className="btn btn-just-icon btn-simple btn-pinterest"><i className="fa fa-pinterest"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xs-2 follow">
                                <button className="btn btn-fab btn-primary" rel="tooltip" title="Follow this user">
                                        <i className="material-icons">add</i>
                                    </button>
                                </div>
                            </div>
            
            
                            <div className="description text-center">
                                <p> Julien Rajerison est un developpeur web, Fondateur de l'association Techzara </p>
                            </div>
            
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="profile-tabs">
                                        <div className="nav-align-center">
                                            <ul className="nav nav-pills nav-pills-icons" role="tablist">
                                                <li className="active">
                                                    <a href="#work" role="tab" data-toggle="tab">
                                                        <i className="material-icons">palette</i>
                                                        Work
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#connections" role="tab" data-toggle="tab">
                                                        <i className="material-icons">people</i>
                                                        Connections
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#media" role="tab" data-toggle="tab">
                                                        <i className="material-icons">camera</i>
                                                        Media
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-content">
                                <div className="tab-pane active work" id="work">
                                    <div className="row">
                                        <div className="col-md-7 col-md-offset-1">
                                            <h4 className="title">Latest Collections</h4>
                                            <div className="row collections">
                                                <div className="col-md-6">
                                                    <div className="card card-background">
                                                        <a href="#pablo"></a>
                                                        <div className="content">
                                                            <label className="label label-primary">Spring 2016</label>
                                                            <a href="#pablo">
                                                                <h2 className="card-title">Stilleto</h2>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="card card-background">
                                                        <a href="#pablo"></a>
                                                        <div className="content">
                                                            <label className="label label-primary">Spring 2016</label>
                                                            <a href="#pablo">
                                                                <h2 className="card-title">High Heels</h2>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="card card-background">
                                                        <a href="#pablo"></a>
                                                        <div className="content">
                                                            <label className="label label-primary">Summer 2016</label>
                                                            <a href="#pablo">
                                                                <h2 className="card-title">Flats</h2>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="card card-background">
                                                        <a href="#pablo"></a>
                                                        <div className="content">
                                                            <label className="label label-primary">Winter 2015</label>
                                                            <a href="#pablo">
                                                                <h2 className="card-title">Men's Sneakers</h2>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-2 col-md-offset-1 stats">
                                            <h4 className="title">Stats</h4>
                                            <ul className="list-unstyled">
                                                <li><b>60</b> Products</li>
                                                <li><b>4</b> Collections</li>
                                                <li><b>331</b> Influencers</li>
                                                <li><b>1.2K</b> Likes</li>
                                            </ul>
                                            <hr />
                                            <h4 className="title">About his Work</h4>
                                            <p className="description">French luxury footwear and fashion. The footwear has incorporated shiny, red-lacquered soles that have become his signature.</p>
                                            <hr />
                                            <h4 className="title">Focus</h4>
                                            <span className="label label-primary">Footwear</span>
                                            <span className="label label-rose">Luxury</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="tab-pane connections" id="connections">
                                    <div className="row">
                                        <div className="col-md-5 col-md-offset-1">
                                            <div className="card card-profile card-plain">
                                                <div className="col-md-5">
                                                    <div className="card-image">
                                                        <a href="#pablo">
                                                            <img className="img" src={julien} />
                                                        </a>
                                                    </div>
                                                </div>
                                                <div className="col-md-7">
                                                    <div className="content">
                                                        <h4 className="card-title">Gigi Hadid</h4>
                                                        <h6 className="category text-muted">Model</h6>
            
                                                        <p className="card-description">
                                                            Don't be scared of the truth because we need to restart the human foundation in truth...
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            
                                        <div className="col-md-5">
                                            <div className="card card-profile card-plain">
                                                <div className="col-md-5">
                                                    <div className="card-image">
                                                        <a href="#pablo">
                                                            <img className="img" src={julien} />
                                                        </a>
                                                    </div>
                                                </div>
                                                <div className="col-md-7">
                                                    <div className="content">
                                                        <h4 className="card-title">Marc Jacobs</h4>
                                                        <h6 className="category text-muted">Designer</h6>
            
                                                        <p className="card-description">
                                                            Don't be scared of the truth because we need to restart the human foundation in truth...
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-5 col-md-offset-1">
                                            <div className="card card-profile card-plain">
                                                <div className="col-md-5">
                                                    <div className="card-image">
                                                        <a href="#pablo">
                                                            <img className="img" src={julien} />
                                                        </a>
                                                    </div>
                                                </div>
                                                <div className="col-md-7">
                                                    <div className="content">
                                                        <h4 className="card-title">Kendall Jenner</h4>
                                                        <h6 className="category text-muted">Model</h6>
            
                                                        <p className="card-description">
                                                            I love you like Kanye loves Kanye. Don't be scared of the truth.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            
                                        <div className="col-md-5">
                                            <div className="card card-profile card-plain">
                                                <div className="col-md-5">
                                                    <div className="card-image">
                                                        <a href="#pablo">
                                                            <img className="img" src={julien} />
                                                        </a>
                                                    </div>
                                                </div>
                                                <div className="col-md-7">
                                                    <div className="content">
                                                        <h4 className="card-title">George West</h4>
                                                        <h6 className="category text-muted">Model/DJ</h6>
            
                                                        <p className="card-description">
                                                            I love you like Kanye loves Kanye.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            
                                    </div>
                                </div>
                                <div className="tab-pane text-center gallery" id="media">
                                    <div className="row">
                                        <div className="col-md-3 col-md-offset-3">
                                            <img src={julien} className="img-rounded" />
                                            <img src={julien} className="img-rounded" />
                                        </div>
                                        <div className="col-md-3">
                                            <img src={julien} className="img-rounded" />
                                            <img src={julien} className="img-rounded" />
                                            <img src={julien} className="img-rounded" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}