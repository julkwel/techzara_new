import React, { Component } from 'react';
import Navbar from '../components/Navbar';
import Blog from '../components/Blog';
import Footer from '../components/Footer'
import Card from '../components/card'
import Header from '../components/headerSection'




class App extends Component {
  render() {
    return (
        <div className="container-fluid">
            <Navbar/>
          <section>
            <Header />
          </section>
          <section>
            <Blog />
          </section>
          <section>
            <Card />
          </section>
            <Footer />
        </div>
    );
  }
}

export default App;
