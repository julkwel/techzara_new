import React,{ Component } from 'react'
import Navbar from '../components/Navbar';

export default class Signup extends Component{
    render() {
        return(
        <div className="container-fluid signup-container">
            <Navbar />
			<div className="row">
    			<div className="col-md-6 col-md-offset-1 container cont-form">
					<div className="card card-signup">
                        <h2 className="card-title text-center">Register</h2>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="social text-center">
                                    <button className="btn btn-just-icon btn-round btn-twitter">
                                        <i className="fa fa-twitter"></i>
                                    </button>
                                    <button className="btn btn-just-icon btn-round btn-dribbble" id="btn-google-login">
                                        <i className="fa fa-dribbble"></i>
                                    </button>
                                    <button className="btn btn-just-icon btn-round btn-facebook" >
                                        <i className="fa fa-facebook"> </i>
                                    </button>
                                    <h4> Ou s'inscrire ici </h4>
                                </div>

								<form className="col-md-12 form text-center" method="" action="">
									<div className="container">
										<div className="input-group">
											<span className="input-group-addon">
												<i className="material-icons">face</i>
											</span>
											<input type="text" id="username" className="form-control" placeholder="First Name..." required />
										</div>

										<div className="input-group">
											<span className="input-group-addon">
												<i className="material-icons">email</i>
											</span>
											<input type="email" id="email" className="form-control" placeholder="Email..." required />
										</div>

										<div className="input-group">
											<span className="input-group-addon">
												<i className="material-icons">lock_outline</i>
											</span>
											<input type="password" id="password" placeholder="Password..." className="form-control" required />
										</div>
										<div className="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" required />
												I agree to the <a href="#something">terms and conditions</a>.
											</label>
										</div>
									</div>
									<div className="footer text-center">
										<a href="javascript:signup();" className="btn btn-info">S'inscrire</a>
									</div>
								</form>
                            </div>
                        </div>
                	</div>
                </div>
            </div>
		</div>
        )
    }
}