import React from 'react';
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker';
import Router from 'react-router-dom/BrowserRouter';
import { AnimatedSwitch } from 'react-router-transition';
import Route from 'react-router-dom/Route';
import Home from './containers/App';
import Signup from './containers/Signup'
import Profil from './components/profilePage'
import 'bootstrap/dist/css/bootstrap.min.css'
import "./css/material-kit.css"
import "./css/custom.css"
import 'bootstrap/dist/js/bootstrap.min.js'
import "./js/material.min.js"
import './../node_modules/jquery-dropdown/jquery-dropdown.js'
import "./js/material-kit.js"


ReactDOM.render(
    <Router>
      <AnimatedSwitch
        atEnter={{ opacity: 0 }}
        atLeave={{ opacity: 0 }}
        atActive={{ opacity: 1 }}
        className="switch-wrapper"
      >
        <Route exact path="/" component={Home} />
        <Route path="/signup" component={Signup} />
        <Route path="/profil" component={Profil} />
      </AnimatedSwitch>
    </Router>
    , document.getElementById('root'));
    registerServiceWorker();
