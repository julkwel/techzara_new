const API_URL = 'http://localhost:8080/api/';

function signup(data = null) {
    if(data === null) {
        data = {
            username: document.getElementById('username').value,
            password: document.getElementById('email').value,
            email: document.getElementById('password').value,
            provider: 'emailpassword'
        };
    }
    fetch(API_URL + 'adduser', {
        method: 'post',
        body: JSON.stringify(data)
    }).then(function(response) {
        return response.json();
    }).then(function(data) {
        console.log(data);
    });
}

function fbsignup() {
    FB.init({
        appId: 1849098195384170,
        cookie: true,  // enable cookies to allow the server to access the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.8' // use graph api version 2.8
    });
    FB.login((response) => {
        console.log(response);
        //signup(data = null);
    });
}

function ggsignup() {
    if (typeof gapi === 'undefined') {
        window.setTimeout(() => {
            ggsignup();
        }, 500);
        return false;
    }
    gapi.load('auth2', () => {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        const auth2 = gapi.auth2.init({
            client_id: '288925975530-utb8d799vt06ft93j03anb6a35t4003h.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });
        auth2.attachClickHandler('btn-google-login', {},
            (googleUser) => {
                signup({
                    username: googleUser.w3.U3,
                    password: googleUser.getAuthResponse().id_token,
                    email: googleUser.w3.U3,
                    provider: 'google'
                });
            }, (error) => {
                // console.error(JSON.stringify(error, undefined, 2));
            });
    });
}
ggsignup();
